export default {
  methods: {
    theDate() {
      let arrMonth = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio',
        'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
      let date = new Date();
      let day = date.getDate();
      let month = date.getMonth();
      let fYear = date.getFullYear();
      return `${day} de ${arrMonth[month + 1]} de ${fYear}`;
    },
  },
};