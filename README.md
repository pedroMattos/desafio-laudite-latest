# desafio-laudite-v

## Clone this repo
```
git clone https://gitlab.com/pedroMattos/desafio-laudite-latest.git
```

## Project setup to install all dependencies
```
npm install
```

### Compiles and hot-reloads for development, initialize a local server
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
